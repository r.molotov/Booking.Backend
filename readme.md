# Booking.Backend
## Run
```sh
dotnet run
```
## Config
* Database:
```sh
/Booking.WebAPI/Booking.db
```
SQLite
* Net:

| protocol | port |
| --- | ----------- |
| http | 5002 |
| https | 5003 |

* WEB.API Config (appconfig.json):
```sh
{
  "DbConnection": "Data Source=Booking.db",
  "AllowedHosts": "*",
  "AuthenticationURL": "https://localhost:5001",
  "AuthenticationClientName": "BookingWebAPI"
}
```
* Swagger:
```sh
https://localhost:5003/swagger/v1/swagger.json
```
| Template | Types | Descripyion |
| ---------------------- | -------------- | ---|
| /api/{version}/Booking | GET, POST, PUT | CRU-operations for booking entry: get all, or add/update one
| /api/{version}/Booking/{id} | DELETE | Deletes booking entry by id
| /api/{version}/Booking/Today | GET | Gets today bookings
| /api/{version}/Booking/My | GET | Gets bookings belongs to current user

* Authentication:

Redirect for **JWT-token** to **5001** (https) or **5000** (http) port at same URL