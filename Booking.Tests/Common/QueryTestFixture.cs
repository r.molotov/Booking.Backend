﻿using System;
using AutoMapper;
using Corp.Application.Common.Mappings;
using Corp.Application.Interfaces;
using Corp.Persistence;
using Xunit;

namespace CorpTests.Common
{
    public class QueryTestFixture: IDisposable
    {
        public BookingDbContext Context;
        public IMapper Mapper;

        public QueryTestFixture()
        {
            Context = BookingContextFactory.Create();
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AssemblyMappingProfile(typeof(IBookingDbContext).Assembly));
            });
            Mapper = configurationProvider.CreateMapper();
        }
        
        public void Dispose() => BookingContextFactory.Destroy(Context);

        [CollectionDefinition("QueryCollection")]
        public class QueryCollection : ICollectionFixture<QueryTestFixture>
        {
            
        }
    }
}