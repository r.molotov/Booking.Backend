﻿using System;
using Corp.Persistence;

namespace CorpTests.Common
{
    public class TestCommandBase: IDisposable
    {
        protected readonly BookingDbContext Context;
        public TestCommandBase() => Context = BookingContextFactory.Create();
        public void Dispose() => BookingContextFactory.Destroy(Context);
    }
}