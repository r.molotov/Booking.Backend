﻿using System;
using Corp.Domain;
using Corp.Persistence;
using Microsoft.EntityFrameworkCore;

namespace CorpTests.Common
{
    public class BookingContextFactory
    {
        public static Guid UserAId = Guid.NewGuid();
        public static Guid UserBId = Guid.NewGuid();

        public static Guid NoteIdForUpdate = Guid.NewGuid();
        public static Guid NoteIdForDelete = Guid.NewGuid();

        public static BookingDbContext Create()
        {
            var options = new DbContextOptionsBuilder<BookingDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new BookingDbContext(options);
            DbInitializer.Initialize(context);

            context.TableBookings.AddRange(
                new TableBooking
                {
                    Id = Guid.Parse("92BD8D5E-4C84-4126-841D-04951524F91B"),
                    UserId = UserAId,
                    StartTime = new DateTime(2021,11,1, 10, 0, 0),
                    EndTime = new DateTime(2021,11,1,11,0,0)
                },
                new TableBooking
                {
                    Id = NoteIdForUpdate,
                    UserId = UserAId,
                    StartTime = new DateTime(2021,11,2, 10, 0, 0),
                    EndTime = new DateTime(2021,11,2,10,30,0)
                },
                new TableBooking
                {
                    Id = NoteIdForDelete,
                    UserId = UserAId,
                    StartTime = new DateTime(2021,11,3, 10, 0, 0),
                    EndTime = new DateTime(2021,11,3,11,0,0)
                },
                
                new TableBooking
                {
                    Id = Guid.Parse("0382A454-3C9A-47C9-880C-CCFF8DE055DE"),
                    UserId = UserBId,
                    StartTime = new DateTime(2021,11,1, 11, 0, 0),
                    EndTime = new DateTime(2021,11,1,12,0,0)
                },
                new TableBooking
                {
                    Id = Guid.Parse("F3BB42CA-73F1-4033-9E03-0F6565BEA9FE"),
                    UserId = UserBId,
                    StartTime = new DateTime(2021,11,2, 11, 0, 0),
                    EndTime = new DateTime(2021,11,2,12,0,0)
                },
                new TableBooking
                {
                    Id = Guid.Parse("11DCF72B-DCEA-49AA-BA0A-345543EF22B2"),
                    UserId = UserBId,
                    StartTime = DateTime.Today + TimeSpan.FromHours(22),
                    EndTime = DateTime.Today + TimeSpan.FromHours(23)
                }
            );
            
            context.SaveChanges();
            return context;
        }

        public static void Destroy(BookingDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}