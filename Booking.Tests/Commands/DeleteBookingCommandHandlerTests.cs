﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Booking.Commands.CreateBooking;
using Corp.Application.Booking.Commands.DeleteBooking;
using Corp.Application.Common.Exceptions;
using CorpTests.Common;
using Xunit;

namespace CorpTests.Commands
{
    public class DeleteBookingCommandHandlerTests: TestCommandBase
    {
        [Fact]
        public async Task DeleteBookingCommandHandler_Success()
        {
            //arrange
            var handler = new DeleteBookingCommandHandler(Context);

            //act
            await handler.Handle(new DeleteBookingCommand
                {
                    Id = BookingContextFactory.NoteIdForDelete,
                    UserId = BookingContextFactory.UserAId
                }, CancellationToken.None
            );

            //assert
            Assert.Null(Context.TableBookings.SingleOrDefault(b => b.Id == BookingContextFactory.NoteIdForDelete));
        }
        
        [Fact]
        public async Task DeleteBookingCommandHandler_FailOnWrongId()
        {
            //arrange
            var handler = new DeleteBookingCommandHandler(Context);

            //assert
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await handler.Handle(new DeleteBookingCommand
                    {
                        Id = Guid.NewGuid(),
                        UserId = BookingContextFactory.UserAId
                    }, CancellationToken.None
                )
            );
        }
        
        [Fact]
        public async Task DeleteBookingCommandHandler_FailOnWrongUserId()
        {
            //arrange
            var handler = new DeleteBookingCommandHandler(Context);
            
            var createHandler = new CreateBookingCommandHandler(Context);
            var startDate1 = DateTime.Today + TimeSpan.FromDays(5) + TimeSpan.FromHours(15);
            var endDate1 = DateTime.Today + TimeSpan.FromDays(5) + TimeSpan.FromHours(16);
            
            var bookingId = await createHandler.Handle(new CreateBookingCommand
            {
                UserId = BookingContextFactory.UserAId,
                StartTime = startDate1,
                EndTime = endDate1
            }, CancellationToken.None);

            //assert
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await handler.Handle(new DeleteBookingCommand
                    {
                        Id = bookingId,
                        UserId = BookingContextFactory.UserBId
                    }, CancellationToken.None
                )
            );
        }
    }
}