﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Booking.Commands.UpdateBooking;
using Corp.Application.Common.Exceptions;
using CorpTests.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CorpTests.Commands
{
    public class UpdateBookingCommandHandlerTests: TestCommandBase
    {
        [Fact]
        public async Task UpdateBookingCommandHandler_Success()
        {
            //arrange
            var handler = new UpdateBookingCommandHandler(Context);
            var updatedStartTime = new DateTime(2021, 11, 2, 10, 0, 0);
            var updatedEndTime = new DateTime(2021, 11, 2, 10, 40, 0);

            //act
            await handler.Handle(new UpdateBookingCommand
            {
                Id = BookingContextFactory.NoteIdForUpdate,
                UserId = BookingContextFactory.UserAId,
                StartTime = updatedStartTime,
                EndTime = updatedEndTime
            }, CancellationToken.None);

            //assert
            Assert.NotNull(await Context.TableBookings.SingleOrDefaultAsync(b =>
                b.Id == BookingContextFactory.NoteIdForUpdate &&
                b.StartTime == updatedStartTime &&
                b.EndTime == updatedEndTime
            ));
        }

        [Fact]
        public async Task UpdateBookingCommandHandler_FailOnWrongId()
        {
            //arrange
            var handler = new UpdateBookingCommandHandler(Context);

            //act

            //assert
            await Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(
                new UpdateBookingCommand
                {
                    Id = Guid.NewGuid(),
                    UserId = BookingContextFactory.UserAId
                }, CancellationToken.None)
            );
        }

        [Fact]
        public async Task UpdateBookingCommandHandler_FailOnWrongUserId()
        {
            //arrange
            var handler = new UpdateBookingCommandHandler(Context);

            //act

            //assert
            await Assert.ThrowsAsync<NotFoundException>(async () => await handler.Handle(
                new UpdateBookingCommand
                {
                    Id = BookingContextFactory.NoteIdForUpdate,
                    UserId = BookingContextFactory.UserBId
                },CancellationToken.None)
            );
        }

        [Fact]
        public async Task UpdateBookingCommandHandler_FailOnAlreadyBooked()
        {
            //arrange
            var handler = new UpdateBookingCommandHandler(Context);
            var updatedEndTime = new DateTime(2021, 11, 2, 11, 30, 0);
            //act
            
            //assert
            await Assert.ThrowsAsync<IncorrectBookingException>(async () => await handler.Handle(
                new UpdateBookingCommand
                {
                    Id = BookingContextFactory.NoteIdForUpdate,
                    UserId = BookingContextFactory.UserAId,
                    EndTime = updatedEndTime
                    
                },CancellationToken.None)
            );
        }
    }
}