﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Booking.Commands.CreateBooking;
using Corp.Application.Common.Exceptions;
using CorpTests.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CorpTests.Commands
{
    public class CreateBookingCommandHandlerTests: TestCommandBase
    {
        [Fact]
        public async Task CreateBookingCommandHandlerTest_Success()
        {
            //arrange
            var handler = new CreateBookingCommandHandler(Context);
            var startDate1 = DateTime.Today + TimeSpan.FromDays(2) + TimeSpan.FromHours(13);
            var endDate1 = DateTime.Today + TimeSpan.FromDays(2) + TimeSpan.FromHours(14);
            var endDate2 = DateTime.Now + TimeSpan.FromHours(1);

            //act
            var bookingId1 = await handler.Handle(new CreateBookingCommand
            {
                UserId = BookingContextFactory.UserAId,
                StartTime = startDate1,
                EndTime = endDate1
            }, CancellationToken.None);
            
            var bookingId2 = await handler.Handle(new CreateBookingCommand
            {
                UserId = BookingContextFactory.UserAId,
                EndTime = endDate2
            }, CancellationToken.None);

            //assert
            Assert.NotNull(await Context.TableBookings.SingleOrDefaultAsync(b =>
                b.Id == bookingId1 &&
                b.StartTime == startDate1 &&
                b.EndTime == endDate1
            ));
            
            Assert.NotNull(await Context.TableBookings.SingleOrDefaultAsync(b =>
                b.Id == bookingId2 &&
                b.StartTime.Date == DateTime.Today.Date &&
                b.EndTime == endDate2
            ));
        }

        [Fact]
        public async Task CreateBookingCommandHandlerTest_FailOnAlreadyBooked()
        {
            //arrange
            var handler = new CreateBookingCommandHandler(Context);
            var startDate1 = DateTime.Today + TimeSpan.FromDays(1) + TimeSpan.FromHours(13);
            var endDate1 = DateTime.Today + TimeSpan.FromDays(1) + TimeSpan.FromHours(14);
            var endDate2 = DateTime.Today + TimeSpan.FromDays(2) + TimeSpan.FromHours(14);

            //act
            
            //assert
            await Assert.ThrowsAsync<IncorrectBookingException>(async () => await handler.Handle(
                new CreateBookingCommand
                {
                    UserId = BookingContextFactory.UserAId,
                    StartTime = new DateTime(2021,11,1,11,30, 0),
                    EndTime = new DateTime(2021,11,1,13,0, 0)
                }, CancellationToken.None)
            );

            await Assert.ThrowsAsync<IncorrectBookingException>(async () => await handler.Handle(
                new CreateBookingCommand
                {
                    UserId = BookingContextFactory.UserBId,
                    StartTime = new DateTime(2021,11,1,9,0, 0),
                    EndTime = new DateTime(2021,11,1,10,30, 0)
                }, CancellationToken.None)
            );
        }
    }
}