﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Application.Booking.Queries.GetUserBookingList;
using Corp.Persistence;
using CorpTests.Common;
using Shouldly;
using Xunit;

namespace CorpTests.Queries
{
    [Collection("QueryCollection")]
    public class GetUserBookingListQueryHandlerTests
    {
        private readonly BookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetUserBookingListQueryHandlerTests(QueryTestFixture fixture) =>
            (_dbContext, _mapper) = (fixture.Context, fixture.Mapper);

        [Fact]
        public async Task GetUserBookingListQueryHandler_Success()
        {
            //arrange
            var handler = new GetUserBookingListQueryHandler(_dbContext, _mapper);

            //act
            var result = await handler.Handle(
                new GetUserBookingListQuery {UserId = BookingContextFactory.UserAId}, CancellationToken.None
            );

            //assert
            result.ShouldBeOfType<BookingListVm>();
            result.Bookings.Count.ShouldBe(3);
            result.Bookings.ShouldAllBe(b => b.IsOwn == true);
        }
    }
}