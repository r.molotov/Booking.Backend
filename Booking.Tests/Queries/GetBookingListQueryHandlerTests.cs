﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Persistence;
using CorpTests.Common;
using Microsoft.AspNetCore.Http.Features;
using Shouldly;
using Xunit;

namespace CorpTests.Queries
{
    [Collection("QueryCollection")]
    public class GetBookingListQueryHandlerTests
    {
        private readonly BookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetBookingListQueryHandlerTests(QueryTestFixture fixture) =>
            (_dbContext, _mapper) = (fixture.Context, fixture.Mapper);

        [Fact]
        public async Task GetBookingListQueryHandler_Success()
        {
            //arrange
            var handler = new GetBookingListQueryHandler(_dbContext, _mapper);

            //act
            var result = await handler.Handle(
                new GetBookingListQuery {UserId = BookingContextFactory.UserAId}, CancellationToken.None
            );

            //assert
            result.ShouldBeOfType<BookingListVm>();
            result.Bookings.Count.ShouldBe(6);
        }
    }
}