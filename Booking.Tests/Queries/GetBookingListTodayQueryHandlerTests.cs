﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Application.Booking.Queries.GetBookingListToday;
using Corp.Persistence;
using CorpTests.Common;
using Shouldly;
using Xunit;

namespace CorpTests.Queries
{
    [Collection("QueryCollection")]
    public class GetBookingListTodayQueryHandlerTests
    {
        private readonly BookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetBookingListTodayQueryHandlerTests(QueryTestFixture fixture) =>
            (_dbContext, _mapper) = (fixture.Context, fixture.Mapper);

        [Fact]
        public async Task GetBookingListTodayQueryHandler_Success()
        {
            //arrange
            var handler = new GetBookingListTodayQueryHandler(_dbContext, _mapper);
            
            //act
            var result = await handler.Handle(
                new GetBookingListTodayQuery {UserId = BookingContextFactory.UserAId}, CancellationToken.None
            );

            //assert
            result.ShouldBeOfType<BookingListVm>();
            result.Bookings.ShouldAllBe(b =>
                b.StartTime.Date == DateTime.Today.Date &&
                b.EndTime.Date == DateTime.Today.Date
            );
            result.Bookings.Count.ShouldBe(1);
        }
    }
}