﻿using System;
using Corp.Application.Interfaces;
using Corp.Domain;
using Corp.Persistence.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Corp.Persistence
{
    public class BookingDbContext : DbContext, IBookingDbContext
    {
        public DbSet<TableBooking> TableBookings { get; set; }
        
        public BookingDbContext(DbContextOptions<BookingDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TableBookingConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}