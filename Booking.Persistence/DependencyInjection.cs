﻿using Corp.Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Corp.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(
            this IServiceCollection services,
            IConfiguration configuration
        )
        {
            var connectionString = configuration["DbConnection"];
            services.AddDbContext<BookingDbContext>(options =>
            {
                options.UseSqlite(connectionString);
            });
            services.AddScoped<IBookingDbContext>(provider =>
            
                provider.GetService<BookingDbContext>()
            );
            
            return services;
        }
    }
}