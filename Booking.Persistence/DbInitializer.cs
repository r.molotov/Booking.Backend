﻿using Microsoft.EntityFrameworkCore;

namespace Corp.Persistence
{
    public class DbInitializer
    {
        public static void Initialize(BookingDbContext dbContext) =>
            dbContext.Database.EnsureCreated();
    }
}