﻿using Corp.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Corp.Persistence.EntityTypeConfigurations
{
    public class TableBookingConfiguration: IEntityTypeConfiguration<TableBooking>
    {
        public void Configure(EntityTypeBuilder<TableBooking> builder)
        {
            builder.HasKey(tb => tb.Id);
            builder.HasIndex(tb => tb.Id).IsUnique();
        }
    }
}