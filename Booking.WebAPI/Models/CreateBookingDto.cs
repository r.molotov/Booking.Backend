﻿using System;
using AutoMapper;
using Corp.Application.Booking.Commands.CreateBooking;
using Corp.Application.Common.Mappings;

namespace Corp.WebAPI.Models
{
    public class CreateBookingDto : IMapWith<CreateBookingCommand>
    {
        public DateTime? StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateBookingDto, CreateBookingCommand>()
                .ForMember(command => command.StartTime,
                    opt => opt.MapFrom(dto => dto.StartTime))
                .ForMember(command => command.EndTime,
                    opt => opt.MapFrom(dto => dto.EndTime));

        }
    }
}