﻿using System;
using AutoMapper;
using Corp.Application.Booking.Commands.UpdateBooking;
using Corp.Application.Common.Mappings;

namespace Corp.WebAPI.Models
{
    public class UpdateBookingDto: IMapWith<UpdateBookingCommand>
    {
        public Guid Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateBookingDto, UpdateBookingCommand>()
                .ForMember(command => command.Id,
                    opt => opt.MapFrom(dto => dto.Id))
                .ForMember(command => command.StartTime,
                    opt => opt.MapFrom(dto => dto.StartTime))
                .ForMember(command => command.EndTime,
                    opt => opt.MapFrom(dto => dto.EndTime));
        }
    }
}