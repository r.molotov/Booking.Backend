﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Corp.Application.Booking.Commands.CreateBooking;
using Corp.Application.Booking.Commands.DeleteBooking;
using Corp.Application.Booking.Commands.UpdateBooking;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Application.Booking.Queries.GetBookingListToday;
using Corp.Application.Booking.Queries.GetUserBookingList;
using Corp.WebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Corp.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class BookingController : BaseController
    {
        private readonly IMapper _mapper;
        public BookingController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets all bookings
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /booking
        /// </remarks>
        /// <returns>Returns BookingListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<BookingListVm>> GetAll()
        {
            var query = new GetBookingListQuery {UserId = UserId};
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets all bookings today
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /Booking/Today
        /// </remarks>
        /// <returns>Returns BookingListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpGet("Today")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<BookingListVm>> GetToday()
        {
            var query = new GetBookingListTodayQuery() {UserId = UserId};
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }
        
        /// <summary>
        /// Gets user's bookings
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /Booking/My
        /// </remarks>
        /// <returns>Returns BookingListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpGet("My")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<BookingListVm>> GetMy()
        {
            var query = new GetUserBookingListQuery() {UserId = UserId};
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates new booking
        /// </summary>
        /// <remarks>
        /// Sample command:
        /// POST /Booking
        /// {
        ///     StartTime: booking start time or null. If null, booking starts from DateTime.Now
        ///     EndTime: when booking ends
        /// }
        /// </remarks>
        /// <param name="dto">CreateBookingDto object</param>
        /// <returns>Returns booking id (guid)</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateBookingDto dto)
        {
            var command = _mapper.Map<CreateBookingCommand>(dto);
            command.UserId = UserId;

            var noteId = await Mediator.Send(command);
            return Ok(noteId);
        }

        /// <summary>
        /// Updates selected booking
        /// </summary>
        /// <remarks>
        /// Sample command:
        /// PUT /Booking
        /// {
        ///     Id: booking id
        ///     StartTime: booking start time or null. If null, booking starts from DateTime.Now
        ///     EndTime: when booking ends   
        /// }
        /// </remarks>
        /// <param name="dto">UpdateBookingDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateBookingDto dto)
        {
            var command = _mapper.Map<UpdateBookingCommand>(dto);
            command.UserId = UserId;

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes booking by id
        /// </summary>
        /// <remarks>
        /// Sample command:
        /// DELETE /Booking/CC6A5F49-11E0-48A2-9419-CCFD97B1BD28
        /// </remarks>
        /// <param name="id">Booking id (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteBookingCommand {Id = id, UserId = UserId};
            await Mediator.Send(command);
            return NoContent();
        }
    }
}