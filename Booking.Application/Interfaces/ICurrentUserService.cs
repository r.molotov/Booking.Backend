﻿using System;

namespace Corp.Application.Interfaces
{
    public interface ICurrentUserService
    {
        Guid UserId { get; }
    }
}