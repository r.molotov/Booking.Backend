﻿using System.Threading;
using System.Threading.Tasks;
using Corp.Domain;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Interfaces
{
    public interface IBookingDbContext
    {
        DbSet<TableBooking> TableBookings { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}