﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Common.Exceptions;
using Corp.Application.Interfaces;
using Corp.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Booking.Commands.CreateBooking
{
    public class CreateBookingCommandHandler: IRequestHandler<CreateBookingCommand, Guid>
    {
        private readonly IBookingDbContext _dbContext;

        public CreateBookingCommandHandler(IBookingDbContext dbContext) =>
            _dbContext = dbContext;
        
        public async Task<Guid> Handle(CreateBookingCommand request, CancellationToken cancellationToken)
        {
            var startTime = request.StartTime ?? DateTime.Now;

            if (await _dbContext.TableBookings
                .AnyAsync(tb => 
                    (request.EndTime > tb.StartTime && request.EndTime < tb.EndTime) ||
                    (startTime > tb.StartTime && startTime < tb.EndTime) ||
                    (startTime <= tb.StartTime && request.EndTime >= tb.EndTime)
                    , cancellationToken)
            )
                throw new IncorrectBookingException(request.EndTime);

            var booking = new TableBooking
            {
                Id = Guid.NewGuid(),
                UserId = request.UserId,
                StartTime = startTime,
                EndTime = request.EndTime
            };

            await _dbContext.TableBookings.AddAsync(booking, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return booking.Id;
        }
    }
}