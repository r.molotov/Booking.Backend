﻿using System;
using FluentValidation;

namespace Corp.Application.Booking.Commands.CreateBooking
{
    public class CreateBookingCommandValidator: AbstractValidator<CreateBookingCommand>
    {
        public CreateBookingCommandValidator()
        {
            RuleFor(command => command.UserId)
                .NotEqual(Guid.Empty);

            RuleFor(command => command.StartTime)
                .NotEmpty()
                .GreaterThan(command => DateTime.Now)
                .When(command => command.StartTime.HasValue);
            
            RuleFor(command => command.EndTime)
                .NotEmpty()
                .GreaterThan(command => DateTime.Now)
                .GreaterThan(command => command.StartTime)
                .When(command => command.StartTime.HasValue, ApplyConditionTo.CurrentValidator);
        }
    }
}