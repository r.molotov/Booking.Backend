﻿using System;
using MediatR;

namespace Corp.Application.Booking.Commands.CreateBooking
{
    public class CreateBookingCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}