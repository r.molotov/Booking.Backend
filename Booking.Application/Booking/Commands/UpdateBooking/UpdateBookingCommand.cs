﻿using System;
using MediatR;

namespace Corp.Application.Booking.Commands.UpdateBooking
{
    public class UpdateBookingCommand: IRequest

    {
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime EndTime { get; set; }
    }
}