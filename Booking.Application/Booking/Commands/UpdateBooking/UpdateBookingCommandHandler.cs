﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Common.Exceptions;
using Corp.Application.Interfaces;
using Corp.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Booking.Commands.UpdateBooking
{
    public class UpdateBookingCommandHandler : IRequestHandler<UpdateBookingCommand>
    {
        private readonly IBookingDbContext _dbContext;
        public UpdateBookingCommandHandler(IBookingDbContext context) => _dbContext = context;

        public async Task<Unit> Handle(UpdateBookingCommand request, CancellationToken cancellationToken)
        {
            var entry =
                await _dbContext.TableBookings.FirstOrDefaultAsync(e=> e.Id == request.Id, cancellationToken: cancellationToken);
            if (entry == null || entry.UserId != request.UserId)
                throw new NotFoundException(nameof(TableBooking), request.Id);
            
            var startTime = request.StartTime ?? DateTime.Now;
            if (await _dbContext.TableBookings
                .Where(b => b.Id != entry.Id) // coz update, ignore self on crossing validation
                .AnyAsync(tb => 
                        (request.EndTime > tb.StartTime && request.EndTime < tb.EndTime) ||
                        (startTime > tb.StartTime && startTime < tb.EndTime) ||
                        (startTime <= tb.StartTime && request.EndTime >= tb.EndTime)
                    , cancellationToken)
            )
                throw new IncorrectBookingException(request.EndTime);

            entry.StartTime = startTime;
            entry.EndTime = request.EndTime;

            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}