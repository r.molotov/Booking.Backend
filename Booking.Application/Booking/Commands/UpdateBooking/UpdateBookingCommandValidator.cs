﻿using System;
using FluentValidation;

namespace Corp.Application.Booking.Commands.UpdateBooking
{
    public class UpdateBookingCommandValidator : AbstractValidator<UpdateBookingCommand>
    {
        public UpdateBookingCommandValidator()
        {
            RuleFor(command => command.Id)
                .NotEqual(Guid.Empty);
            
            RuleFor(command => command.UserId)
                .NotEqual(Guid.Empty);

            RuleFor(command => command.StartTime)
                .NotEmpty()
                .GreaterThan(command => DateTime.Now)
                .When(command => command.StartTime.HasValue);
            
            RuleFor(command => command.EndTime)
                .NotEmpty()
                .GreaterThan(command => DateTime.Now)
                .GreaterThan(command => command.StartTime)
                .When(command => command.StartTime.HasValue, ApplyConditionTo.CurrentValidator);
        }
    }
}