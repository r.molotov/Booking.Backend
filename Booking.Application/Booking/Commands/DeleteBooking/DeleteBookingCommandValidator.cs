﻿using System;
using FluentValidation;

namespace Corp.Application.Booking.Commands.DeleteBooking
{
    public class DeleteBookingCommandValidator: AbstractValidator<DeleteBookingCommand>
    {
        public DeleteBookingCommandValidator()
        {
            RuleFor(command => command.Id)
                .NotEqual(Guid.Empty);
            RuleFor(command => command.UserId)
                .NotEqual(Guid.Empty);
        }
    }
}