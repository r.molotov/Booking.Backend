﻿using System;
using MediatR;

namespace Corp.Application.Booking.Commands.DeleteBooking
{
    public class DeleteBookingCommand : IRequest
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
    }
}