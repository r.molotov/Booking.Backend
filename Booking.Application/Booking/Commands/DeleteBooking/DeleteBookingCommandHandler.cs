﻿using System.Threading;
using System.Threading.Tasks;
using Corp.Application.Common.Exceptions;
using Corp.Application.Interfaces;
using Corp.Domain;
using MediatR;

namespace Corp.Application.Booking.Commands.DeleteBooking
{
    public class DeleteBookingCommandHandler : IRequestHandler<DeleteBookingCommand>
    {
        private readonly IBookingDbContext _dbContext;
        public DeleteBookingCommandHandler(IBookingDbContext dbContext) => _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteBookingCommand request, CancellationToken cancellationToken)
        {
            var entry =
                await _dbContext.TableBookings.FindAsync(new object[] {request.Id}, cancellationToken);
            if (entry == null || entry.UserId != request.UserId)
                throw new NotFoundException(nameof(TableBooking), request.Id);

            _dbContext.TableBookings.Remove(entry);
            await _dbContext.SaveChangesAsync(cancellationToken);
            
            return Unit.Value;
        }
    }
}