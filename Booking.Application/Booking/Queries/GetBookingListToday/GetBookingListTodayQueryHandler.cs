﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Booking.Queries.GetBookingListToday
{
    public class GetBookingListTodayQueryHandler: IRequestHandler<GetBookingListTodayQuery, BookingListVm>
    {
        private readonly IBookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetBookingListTodayQueryHandler(IBookingDbContext dbContext, IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);
        
        public async Task<BookingListVm> Handle(GetBookingListTodayQuery request, CancellationToken cancellationToken)
        {
            var bookingQuery = await _dbContext.TableBookings
                .Where(booking => booking.StartTime.Date == DateTime.Today.Date || booking.EndTime.Date == DateTime.Today.Date)
                .ProjectTo<BookingLookupDto>(_mapper.ConfigurationProvider, new { requestUserId = request.UserId })
                .ToListAsync(cancellationToken);

            return new BookingListVm {Bookings = bookingQuery};
        }
    }
}