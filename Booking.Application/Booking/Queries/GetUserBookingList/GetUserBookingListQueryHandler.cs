﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Corp.Application.Booking.Queries.GetBookingList;
using Corp.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Booking.Queries.GetUserBookingList
{
    public class GetUserBookingListQueryHandler : IRequestHandler<GetUserBookingListQuery, BookingListVm>
    {
        private readonly IBookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetUserBookingListQueryHandler(IBookingDbContext dbContext, IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);
        
        public async Task<BookingListVm> Handle(GetUserBookingListQuery request, CancellationToken cancellationToken)
        {
            var bookingQuery = await _dbContext.TableBookings
                .Where(booking => booking.UserId == request.UserId)
                .ProjectTo<BookingLookupDto>(_mapper.ConfigurationProvider, new { requestUserId = request.UserId })
                .ToListAsync(cancellationToken);

            return new BookingListVm {Bookings = bookingQuery};
        }
    }
}