﻿using System;
using FluentValidation;

namespace Corp.Application.Booking.Queries.GetBookingList
{
    public class GetBookingListQueryValidator : AbstractValidator<GetBookingListQuery>
    {
        public GetBookingListQueryValidator()
        {
            RuleFor(booking => booking.UserId)
                .NotEqual(Guid.Empty);
        }
    }
}