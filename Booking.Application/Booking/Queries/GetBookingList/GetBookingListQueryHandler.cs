﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Corp.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Corp.Application.Booking.Queries.GetBookingList
{
    public class GetBookingListQueryHandler: IRequestHandler<GetBookingListQuery, BookingListVm>
    {
        private readonly IBookingDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetBookingListQueryHandler(IBookingDbContext dbContext, IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);
        
        public async Task<BookingListVm> Handle(GetBookingListQuery request, CancellationToken cancellationToken)
        {
            var bookingQuery = await _dbContext.TableBookings
                .ProjectTo<BookingLookupDto>(_mapper.ConfigurationProvider, new { requestUserId = request.UserId })
                .ToListAsync(cancellationToken);

            return new BookingListVm {Bookings = bookingQuery};
        }
    }
}