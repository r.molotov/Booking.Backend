﻿using System;
using MediatR;

namespace Corp.Application.Booking.Queries.GetBookingList
{
    public class GetBookingListQuery: IRequest<BookingListVm>
    {
        public Guid UserId { get; set; }
    }
}