﻿using System.Collections.Generic;

namespace Corp.Application.Booking.Queries.GetBookingList
{
    public class BookingListVm
    {
        public IList<BookingLookupDto> Bookings { get; set; }
    }
}