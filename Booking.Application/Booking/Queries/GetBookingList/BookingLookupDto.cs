﻿using System;
using AutoMapper;
using Corp.Application.Common.Mappings;
using Corp.Domain;

namespace Corp.Application.Booking.Queries.GetBookingList
{
    public class BookingLookupDto: IMapWith<TableBooking>
    {
        public Guid Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsOwn { get; set; }

        public void Mapping(Profile profile)
        {
            Guid requestUserId = Guid.Empty;
            profile.CreateMap<TableBooking, BookingLookupDto>()
                .ForMember(dto => dto.Id,
                    opt => opt.MapFrom(bt => bt.Id))
                .ForMember(dto => dto.StartTime,
                    opt => opt.MapFrom(bt => bt.StartTime))
                .ForMember(dto => dto.EndTime,
                    opt => opt.MapFrom(bt => bt.EndTime))
                .ForMember(dto => dto.IsOwn,
                    opt => opt.MapFrom(bt => bt.UserId == requestUserId))
                ;
        }
    }
}