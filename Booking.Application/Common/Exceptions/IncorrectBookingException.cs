﻿using System;

namespace Corp.Application.Common.Exceptions
{
    public class IncorrectBookingException: Exception
    {
        public IncorrectBookingException(DateTime time) :
            base($"Can't set booking time at {time}, because it's already booked by somebody")
        {
            
        }
    }
}